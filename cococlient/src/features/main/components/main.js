import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Home from '../../home/components/home';
import About from '../../about/components/about';
import AlgorithmPage from '../../form/containers/algorithmPage';
import Catalog from '../../catalog/containers/catalog';

const Main = (props) => (
  <main className={props.className}>
    <Route exact path='/' component={Home} />
    <Route exact path='/catalog' component={Catalog} />
    <Route path='/form' component={AlgorithmPage} />
    <Route path='/about' component={About} />
  </main>
)

export default Main;
