import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
// import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    fontWeight: '600',
    color: "#29AFF0"
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 200,
  },
});

// textField api: https://material-ui.com/api/text-field/
// html types: https://www.w3schools.com/html/html_form_input_types.asp

const renderTextField = (index, element, isValid, props) => {
  let key   = element[0];
  let value = element[1];
  return (
    <TextField
      key={key}
      id={index.toString()}
      label={value.label}
      margin="dense"
      fullWidth
      error={isValid}
      helperText=""
      variant="outlined"
      className={props.classes.textField}
      onChange={(event) => props.handleChange(key, event.target.value)}
    />
  );
}

const DynamicInputForm = (props) => {

  console.log("input in input: ", props);

  const inputs = Object.entries(props.inputs);
  return (
    <div>
      <form className={props.classes.container} noValidate autoComplete="off">
        {
          (inputs.length !== 0) ? (
            inputs.map((element, index) => {
              return renderTextField(index, element, false, props);  
            })) : <p>No inputs found</p>
        }
      </form>
    </div>
  );
};

DynamicInputForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DynamicInputForm);
