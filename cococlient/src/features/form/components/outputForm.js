import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';

const styles = (theme) => ({

  container: {
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    fontWeight: '600',
    color: "#29AFF0",
  },
  dense: {
    marginTop: 19
  },
  menu: {
    width: 200,
  },
});

// textField api: https://material-ui.com/api/text-field/
// html types: https://www.w3schools.com/html/html_form_input_types.asp

const renderTextField = (index, element, isValid, style) => {
  let key   = element[0];
  let value = element[1];
  return (
    <TextField
      key={key}
      id={index.toString()}
      label={value.label}
      margin="dense"
      fullWidth
      error={isValid}
      helperText=""
      InputProps={{
        readOnly: true
      }}
      variant="outlined"
      className={style}
    />
  );
}

const DynamicOutputForm = (props) => {
  console.log("output in output: ", props);
  
  const outputs = Object.entries(props.outputs);
  return (
    <div>
      <form className={props.classes.container} noValidate autoComplete="off">
        {
          (outputs.length !== 0) ? (
            outputs.map((element, index) => {
              return renderTextField(index, element, false, props.classes.textField);  
            })) : <p>No outputs found</p>
        }
      </form>
    </div>
  );
};

DynamicOutputForm.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DynamicOutputForm);
