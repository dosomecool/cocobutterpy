import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import DynamicInputForm from '../components/inputForm';
import DynamicOutputForm from '../components/outputForm';
import { ServerApi } from "../../../api/CocoApi";

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    marginTop: "16px",
    marginLeft: "15%",
    marginRight: "15%",
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: "#1E1E1E",
    height: "86vh",
    backgroundColor: "#FFFFFF"
  },
});

class AlgorithmPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      inputs: {},
      outputs: {},
      errors: {}
    };

    // this.retrieveModel = this.retrieveModel.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  // async retrieveModel(model) {

  //   try {
  //     const response = await ServerApi.get(`/catalog/${model}`, {
  //       params: {
  //         id: this.props.algorithm.information.id
  //       }
  //     });

  //     const data = response.data;

  //     if (model === "inputs") {
  //       this.setState({ inputs: data });

  //     } else if (model === "outputs") {
  //       this.setState({ outputs: data });
  //     }

  //   } catch (e) {
  //     console.log(`Failed to retrieve ${model} model: ${e}`);
  //   }
  // }

  handleChange(key, value) {
    let newInputs = Object.assign({}, this.state.inputs);
    newInputs[key].value = { value: [value] };
    this.setState({ inputs: newInputs });
    // var array = JSON.parse("[" + string + "]");

  }

  async submitInput() {

    try {
      const data = JSON.stringify(this.state.inputs);

      const request = await ServerApi.post('/catalog/input', data, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      });
      const result = request.data; 
      console.log("result from post: ", result);

      if (result !== "success") {
        this.setState({ errors: result });
        console.log("errors: ", this.state.errors);
      }

    } catch (e) {
      console.log(`Failed to send inputs: ${e}`);
    }
  }

  componentDidMount() {
    // this.retrieveModel("inputs");
    // this.retrieveModel("outputs");
  }

  shouldComponentUpdate(nextProps, nextState) {
    return true;
  }

  componentWillUnmount() {
    // clearInterval(this.interval);
  }

  render() {
    const { classes } = this.props;
    console.log("props in algorithm page: ", this.props);

    return (
      <div className={classes.root}>
        <Grid container direction="row" spacing={24}>

          <Grid item xs={8} style={{ minWidth: "365px" }}>
            <Paper className={classes.paper}>
              <DynamicOutputForm outputs={this.props.algorithm.output} />
            </Paper>
          </Grid>

          <Grid item xs={4} style={{ minWidth: "365px" }}>
            <Paper className={classes.paper}>
              <DynamicInputForm inputs={this.props.algorithm.input} handleChange={this.handleChange} />
              <Button color="inherit" >Clear</Button>
              <Button color="inherit" onClick={() => this.submitInput()}>Submit</Button>
            </Paper>
          </Grid>

        </Grid>
      </div>
    );
  }
}

AlgorithmPage.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  return { algorithm: state.algorithm }
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    // ...userInputActions
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AlgorithmPage));
