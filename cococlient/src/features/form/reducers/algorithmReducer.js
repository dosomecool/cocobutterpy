import * as types from '../../../constants/actionTypes';

const algorithmReducer = (state={}, action) => {
  
  switch (action.type) {
    case types.SELECT_ALGORITHM_SUCCESS:

      console.log("algorithm in reducer: ", action.payload);


      return action.payload;

    default:
      return state;
  }
};

export default algorithmReducer;