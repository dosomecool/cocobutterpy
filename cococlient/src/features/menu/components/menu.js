import React from 'react';
import { push } from 'connected-react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import icon from '../../../resources/images/icon-blue.svg';

const styles = {
  root: {
    flexGrow: 1,
    color: '#007ACC'
  },
  grow: {
    flexGrow: 2,
    textAlign: 'left',
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

const homePage = '/';
const catalogPage = '/catalog';
const aboutPage = '/about';


const MenuBar = (props) => {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <AppBar style={{ backgroundColor: '#007ACC' }}>
        <Toolbar>
          <img src={icon} alt="Icon" style={{ margin: '10px' }}/>
          <Typography variant="h6" color="inherit" className={classes.grow}>
            CocobutterPy
          </Typography>
          <Button color="inherit" onClick={() => props.changePage(homePage)}>Home</Button>
          <Button color="inherit" onClick={() => props.changePage(catalogPage)}>Catalog</Button>
          <Button color="inherit" onClick={() => props.changePage(aboutPage)}>About</Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}

MenuBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  // prop: state.<name>
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  { changePage: (page) => push(page) }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(MenuBar));
