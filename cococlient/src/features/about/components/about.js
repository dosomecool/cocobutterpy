import React from 'react';
import Typography from '@material-ui/core/Typography';

const About = (props) => {
  return (
    <div>
      <Typography variant="h6" align="center" color="secondary" paragraph={true}>
        This is CocobutterPy about page
      </Typography>
    </div>
  );
};

export default About;
