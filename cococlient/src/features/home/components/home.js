import React from 'react';
import Typography from '@material-ui/core/Typography';
import logo from '../../../resources/images/cocobutterpy-orange.svg';

const Home = (props) => {
  return (
    <div align="center">
      <span>
        <img src={logo} alt="Logo" />
      </span>

      <Typography variant="h4" color="inherit">
        Ma main man and lead developer Kevin the gary
      </Typography>
    </div>
  );
};

export default Home;
