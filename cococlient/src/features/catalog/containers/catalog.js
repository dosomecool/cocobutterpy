import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import DisplayCard from '../components/displayCard';
import { ServerApi } from "../../../api/CocoApi";

class Catalog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      catalog: []
    };

    this.display = this.display.bind(this);

    this.text = {
      color: '#ffffff'
    };
  }

  display(catalog) {
    var cards = [];
    catalog.forEach((product, index) => {

      console.log("product in catalog: ", product);

      cards.push(
        <DisplayCard
          key={index}
          id={product.information.id}
          title={product.information.title}
          image={product.information.image}
          description={product.information.description}
          url={product.information.url}
          product={product}
        />
      )
    });
    return cards
  }

  async componentDidMount() {

    try {
      // Load async data from the catalog endpoint.
      const response = await ServerApi.get("/catalog/products");
      this.setState({ catalog: response.data });
    } catch (e) {
      console.log(`Failed to retrieve products: ${e}`);
    }
  }

  render() {
    return (
      <div className="container">
        {(this.state.catalog.length > 0) ? (
          <div>
            {this.display(this.state.catalog)}
          </div>
        ) : <p style={{ textAlign: "center" }}>No products found</p>}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return { catalog: state.catalog }
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    // ...userInputActions
  }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Catalog);


{/* <div>
{this.props.catalog ? <div>{ this.display(this.props.catalog) }</div> 
: <div><p style={ this.text }>Booo no products yet!!!</p></div>}
</div> */}