import React from 'react';
import { push } from 'connected-react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button } from 'reactstrap';
import { selectAlgorithm } from '../../../actions/cocoActions';

const formPage = '/form';

const DisplayCard = (props) => {
  return (
    <div >
      <div className="card">
      <Card>
        <CardImg top width="100%" src={props.image} alt="Card image cap" />
        <CardBody>
          <CardTitle style={{color:"black", fontWeight:"bold"}}>{props.title}</CardTitle>
          <CardText style={{color:"black"}}>{props.description}</CardText>
          <Button size="sm" color="primary"
            onClick={() => {
              props.changePage(formPage);
              props.selectProduct(props.product);
            }}>Start</Button>{' '}
          <Button size="sm" color="primary" href={props.url} target="_blank">Learn more</Button>
        </CardBody>
      </Card>
      </div>
      <div>
        <span>hello</span>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  // prop: state.<name>
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
    { 
      changePage: (page) => push(page),
      selectProduct: (product) => selectAlgorithm(product)
    }, dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(DisplayCard);