import React from 'react'
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { Switch } from 'react-router' // react-router v4/v5
import { ConnectedRouter } from 'connected-react-router'
import configureStore, { history } from './store';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

const store = configureStore(); /* provide initial state if any */

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}> { /* place ConnectedRouter under Provider */}
      <Switch>
        <App />
      </Switch>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
)
