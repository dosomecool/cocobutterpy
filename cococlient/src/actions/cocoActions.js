import * as types from '../constants/actionTypes';

export function selectAlgorithmSuccess(product) {
  return { type: types.SELECT_ALGORITHM_SUCCESS, payload: product };
}

export function showLogoSuccess(isOpen) {
  return { type: types.SHOW_LOGO_SUCCESS, payload: isOpen };
}

export function selectAlgorithm(product) {
  return (dispatch) => {
    dispatch(selectAlgorithmSuccess(product));
  };
}
