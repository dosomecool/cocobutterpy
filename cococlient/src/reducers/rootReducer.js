import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import initialStateReducer from './initialStateReducer';
import algorithmReducer from '../features/form/reducers/algorithmReducer';

export default (history) => combineReducers({
  router: connectRouter(history),
  initialState: initialStateReducer,
  algorithm: algorithmReducer
})
