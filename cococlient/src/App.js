import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MenuBar from './features/menu/components/menu';
import Main from './features/main/components/main';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div>
        <header className="App-header">
          <MenuBar />
        </header>
        <Main className="App-body">
          <img src={logo} className="App-logo" alt="logo" />
        </Main>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  router: state.router
});

const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    // Actions
  }, dispatch
);
export default connect(mapStateToProps, mapDispatchToProps)(App);
