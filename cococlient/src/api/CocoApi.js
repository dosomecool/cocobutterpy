import axios from "axios";

// source: https://designrevision.com/react-axios/

const serverUrl = "http://127.0.0.1:8080";

export const ServerApi = axios.create({
  baseURL: serverUrl,
  responseType: "json",
  timeout: 10000
});

const wikiUrl = "https://en.wikipedia.org";

export const WikiApi = axios.create({
  baseURL: wikiUrl,
  responseType: "json",
  timeout: 10000,
});

// crossDomain: true,
// headers: {
//   "Content-Type": "application/json",
//   'Access-Control-Allow-Origin': '*'
// },
// mode: "cors"

// How to use:

// get
// try {
//   // Load async data from an inexistent endpoint.
//   let userData = await API.get("/inexistent-endpoint");
// } catch (e) {
//   console.log(`Axios request failed: ${e}`);
// }

// post
// axios.post(url[, data[, config]])

// try {
//   const response = await axios.post('http://demo0725191.mockable.io/post_data', { posted_data: 'example' });
//   console.log('Returned data:', response);
// } catch (e) {
//   console.log(`Axios request failed: ${e}`);
// }

// patch 
// axios.patch(url[, data[, config]])

// try {
//   const response = await axios.patch('http://demo0725191.mockable.io/patch_data', { new_name: 'My new cool name!' });
//   console.log('Returned data:', response);
// } catch (e) {
//   console.log(`Axios request failed: ${e}`);
// }

// delete 
// axios.delete(url[, config])

// try {
//   const response = await axios.delete('http://demo0725191.mockable.io/delete_data');
//   console.log('👉 Returned data:', response);
// } catch (e) {
//   console.log(`😱 Axios request failed: ${e}`);
// }