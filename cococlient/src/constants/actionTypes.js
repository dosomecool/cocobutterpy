export const SELECT_ALGORITHM_SUCCESS = 'SELECT_ALGORITHM_SUCCESS';
export const UPDATE_INPUT_SUCCESS = 'UPDATE_INPUT_SUCCESS';
export const UPDATE_OUTPUT_SUCCESS = 'UPDATE_OUTPUT_SUCCESS';
export const SHOW_LOGO_SUCCESS = 'SHOW_LOGO_SUCCESS';
