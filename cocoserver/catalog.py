import cherrypy                               
from model.simpleLinearRegression import SimpleLinearRegression


class Catalog(object):

    # Update this when adding a new algorithm
    def __init__(self):
        self._algorithms = {
            "simple-linear-regression": SimpleLinearRegression
        }

    def products(self):
        products = []
        for algorithm in self._algorithms:
            products.append(self._product(algorithm).model)
        return products

    def model(self, id):
        return self._algorithms.get(id, "Algorithm does not exist!").model

    def _product(self, id):
        return self._algorithms[id]
