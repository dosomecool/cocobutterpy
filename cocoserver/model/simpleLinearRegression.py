import json
import numpy as np
from sklearn.linear_model import LinearRegression

# source: https://realpython.com/linear-regression-in-python/


class SimpleLinearRegression(object):

    model = {
        "information": {
            "id": "simple-linear-regression",
            "title": "Simple Linear Regression",
            "image": "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Linear_"
            + "regression.svg/876px-Linear_regression.svg.png",
            "description": "The simple linear regression model is represented like this: "
            + "y = (β0 + β1 + e). By mathematical convention, the two factors that are "
            + "involved in a simple linear regression analysis are designated x and y. "
            + "The equation that describes how y is related to x is known as the regression model.",
            "url": "https://en.wikipedia.org/wiki/Simple_linear_regression"
        },
        "input":  {
            "x": {
                "value": "",
                "label": "X",
                "hint": 'Use single array i.e.: "[1, 2, 3...]"'
            },
            "y": {
                "value": "",
                "label": "Y",
                "hint": 'Use single array i.e.: "[1, 2, 3...]"'
            }
        },
        "output": {
            "b0": {
                "value": "",
                "label": "B0",
                "hint": "This is the intercept!"
            },
            "b1": {
                "value": "",
                "label": "B1",
                "hint": "This is the slope!"
            },
            "r_sq": {
                "value": "",
                "label": "R-squared",
                "hint": "This is the goodness of fit coef!"
            }
        }
    }

    def __init__(self, inputs={}):
        self._inputs = inputs

    @property
    def inputs(self):
        return [self._x, self._y]

    @inputs.setter
    def inputs(self, data):
        if len(x) == 0:
            raise ValueError("x[] cannot be empty!")

        if len(y) == 0:
            raise ValueError("y[] cannot be empty!")

        self._x = data.x
        self._y = data.y

    def calculate(self):
        # x - is required two be a two dimensional array
        # reshape((-1, 1) - transforms the array from i to 2 dims
        self._x = np.array(self._x).reshape((-1, 1))
        self._y = np.array(self._y)

        # This statement creates the variable model as the instance of LinearRegression.
        # You can provide several optional parameters to LinearRegression:
        # - fit_intercept is a Boolean (True by default) that decides whether to calculate the
        # intercept 𝑏₀ (True) or consider it equal to zero (False).
        # - normalize is a Boolean (False by default) that decides whether to normalize the
        # input variables (True) or not (False).
        # - copy_X is a Boolean (True by default) that decides whether to copy (True) or
        # overwrite the input variables (False).
        # - n_jobs is an integer or None (default) and represents the number of jobs used in
        # parallel computation. None usually means one job and -1 to use all processors.
        self._model = LinearRegression().fit(self.x, self.y)

        # intercept
        self._b0 = self._model.intercept_

        # slope
        self._b1 = self._model.coef_

        # results - r^2 value
        self._r_sq = self._model.score(self.x, self.y)

    @property
    def outputs(self):
        return [self._b0, self._b1, self._r_sq]

    def predict(self, x):
        # predict
        return self._model.predict(x)
