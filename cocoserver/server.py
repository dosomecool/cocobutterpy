import os
import os.path
import random
import string
import json
import cherrypy
from controller import Controller


class Server(object):

    @cherrypy.expose
    def index(self):
        return open('../cococlient/build/index.html')


def CORS():
    if cherrypy.request.method == 'OPTIONS':
        cherrypy.response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PATCH, PUT, DELETE, OPTIONS'
        cherrypy.response.headers['Access-Control-Allow-Headers'] = 'Origin, Content-Type, Accept, Access-Control-Allow-Headers'
        cherrypy.response.headers['Access-Control-Allow-Origin'] = 'http://127.0.0.1:3000'
        cherrypy.response.headers['Access-Control-Max-Age'] = '3600'
        return True
    else:
        cherrypy.response.headers['Access-Control-Allow-Origin'] = 'http://127.0.0.1:3000'

def cors_tool():
    '''
    Handle both simple and complex CORS requests
   
    Add CORS headers to each response. If the request is a CORS preflight
    request swap out the default handler with a simple, single-purpose handler
    that verifies the request and provides a valid CORS response.
    '''
    req_head = cherrypy.request.headers
    resp_head = cherrypy.response.headers

    # Always set response headers necessary for 'simple' CORS.
    resp_head['Access-Control-Allow-Origin'] = req_head.get('Origin', '*')
    resp_head['Access-Control-Expose-Headers'] = 'GET, POST'
    resp_head['Access-Control-Allow-Credentials'] = 'true'
   
    # Non-simple CORS preflight request; short-circuit the normal handler.
    if cherrypy.request.method == 'OPTIONS':
        ac_method = req_head.get('Access-Control-Request-Method', None)
  
        allowed_methods = ['GET', 'POST']
        allowed_headers = [
               'Content-Type',
               'X-Auth-Token',
               'X-Requested-With',
        ]
   
        if ac_method and ac_method in allowed_methods:
            resp_head['Access-Control-Allow-Methods'] = ', '.join(allowed_methods)
            resp_head['Access-Control-Allow-Headers'] = ', '.join(allowed_headers)
  
            resp_head['Connection'] = 'keep-alive'
            resp_head['Access-Control-Max-Age'] = '3600'
   
        # CORS requests should short-circuit the other tools.
        cherrypy.response.body = ''.encode('utf8')
        cherrypy.response.status = 200
        cherrypy.serving.request.handler = None
   
        # Needed to avoid the auth_tool check.
        if cherrypy.request.config.get('tools.sessions.on', False):
            cherrypy.session['token'] = True
        return True

if __name__ == '__main__':
    conf = {
        '/': {
            'tools.staticdir.root': os.path.abspath(os.getcwd())
        },
        '/catalog': {
            'tools.CORS.on': True,
            'tools.response_headers.on': True
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': '../cococlient/build/static'
        }
    }

    server = Server()
    server.catalog = Controller()
    cherrypy.tools.CORS = cherrypy.Tool('before_handler', cors_tool)

    cherrypy.quickstart(server, '/', conf)
