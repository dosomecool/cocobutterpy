import cherrypy
import json
from cherrypy import tools
from catalog import Catalog

# create wiki queries: https://en.wikipedia.org/wiki/Special:ApiSandbox#action=query&format=json&prop=extracts&titles=simple%20linear%20regression&exintro=1


class Controller(object):

    # Update this when adding a new algorithm
    def __init__(self):
        self._catalog = Catalog()
        self._selected = None

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def products(self):
        return self._catalog.products()

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def info(self, id):
        return self._catalog.info(id)

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def inputs(self, id):
        self._selected = type(self._catalog.product(id))
        return self._catalog.inputs(id)

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def outputs(self, id):
        return self._catalog.outputs(id)

    # @cherrypy.expose
    # @cherrypy.tools.json_out()
    # def select(self, id):
    #     self._selected = type(self._catalog.product(id))
    #     print("*******************************************algorithm: ", self._selected)
    #     return {"status":"200"}
    
    # method 2
    @cherrypy.expose
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def input(self):
        data = cherrypy.request.json
        self._selected.inputs(data)
        return {"status":"201"}

    # method 3
    # @cherrypy.expose
    # def input(self):
    #     length = cherrypy.request.headers['Content-Length']
    #     rawbody = cherrypy.request.body.read(int(length))
    #     body = simplejson.loads(rawbody)
    #     print("data in: ", body["name"])

    # @cherrypy.expose
    # def input(self):
    #     length = cherrypy.request.headers.get('Content-Length')
    #     print("length: ", length)
    #     rawbody = cherrypy.request.body.read(int(length))
    #     body = simplejson.loads(rawbody)
    #     print("data in: ", body["name"])

    # @cherrypy.expose
    # @cherrypy.tools.json_in()
    # @cherrypy.tools.json_out()
    # def output(self, **params):
    #     # get formatted output
    #     return "201"

# http://en.wikipedia.org/w/api.php?action=query&format=json&prop=extracts&titles=simple%20linear%20regression&exintro=1

# https://stackoverflow.com/questions/3743769/how-to-receive-json-in-a-post-request-in-cherrypy
